
const EnvironmentVariables = {
  host: 'localhost',
  user: 'mysqlUser',
  password: 'password',
  database: 'databaseName'
}

module.exports.EnvironmentVariables = EnvironmentVariables